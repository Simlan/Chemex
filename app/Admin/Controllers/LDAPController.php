<?php

namespace App\Admin\Controllers;

use Adldap\Auth\BindException;
use Adldap\Auth\PasswordRequiredException;
use Adldap\Auth\UsernameRequiredException;
use App\Http\Controllers\Controller;
use App\Services\LDAPService;
use App\Support\LDAP;

class LDAPController extends Controller
{
    /**
     * AD登录验证
     * @return bool|string
     */
    public function test()
    {
        try {
            if (!admin_setting('ad_enabled')) {
                return -3;
            }
            return LDAP::auth();
        } catch (BindException $e) {
            return $e->getMessage();
        } catch (PasswordRequiredException $e) {
            return -1;
        } catch (UsernameRequiredException $e) {
            return -2;
        }
    }

    public function testMode()
    {
        return LDAPService::importStaffRecords();
    }
}
